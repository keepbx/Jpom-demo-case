package cn.keepbx;

import java.lang.annotation.*;

/**
 * @author jiangzeyin
 * @date 2019/5/9
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TestAnnotation {
}
