@echo off
@REM setlocal enabledelayedexpansion
@REM Set environment variables to prevent some servers from failing to taskkill
set PATH = %PATH%;C:\Windows\system32;C:\Windows;C:\Windows\system32\Wbem

set Tag=Jpom-demo-case-%PROJECT_ID%
set MainClass=org.springframework.boot.loader.JarLauncher
set Lib=%PROJECT_PATH%\lib\
set Log=%PROJECT_PATH%/run.log
set JVM=-server -Xms200m -Xmx400m
set ARGS=


IF "%1"=="start" call:start
IF "%1"=="stop" call:stop
IF "%1"=="status" call:status
IF "%1"=="reload" call:reload


@REM start
:start
    if "%JAVA_HOME%"=="" (
        echo please configure [JAVA_HOME] environment variable
        EXIT 2
    )

	echo Starting.....%Lib%
  java -version
	javaw %JVM% -Dapplication=%Tag% -Djava.ext.dirs=%Lib%  %MainClass% %ARGS% %1 >> %Log%
	@REM timeout 3 > NUL
 ping 127.0.0.1 -n 3 > nul
	EXIT 0
goto:eof


@REM stop
:stop
	for /f "tokens=1 delims= " %%I in ('jps -v ^| findstr "Jpom-demo-case-%PROJECT_ID%"') do taskkill /F /PID %%I
	EXIT 0
goto:eof

@REM view status
:status
	for /f "tokens=1 delims= " %%I in ('jps -v ^| findstr "Jpom-demo-case-%PROJECT_ID%"') do @echo running:%%I

	EXIT 0
goto:eof

@REM reload
:reload
	echo "开始 reload"
	@REM timeout 3 > NUL
    ping 127.0.0.1 -n 2 > nul
	echo "reload done"
	EXIT 0
goto:eof
